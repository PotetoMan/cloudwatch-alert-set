import boto3
import alerm_parameter

def lambda_handler(event, context):
    
    # alerm_parameter.pyのalerm_nameに格納されたものに対して実行
    for alerm_name in alerm_parameter.alerm_name:
        client = boto3.client('cloudwatch')
        cloudwatch = boto3.resource('cloudwatch')
        
        # alerm start
        alarm_slowquery = cloudwatch.Alarm(alerm_name)
        alarm_slowquery.enable_actions()
    
        # 実行結果をログに記録
        response_slowquery = client.describe_alarms(AlarmNames=[alerm_name]) 
        print(response_slowquery)